FROM        openjdk:15-alpine

LABEL       author="Manuel Götte" maintainer="info@six-gaming.com"

RUN         apk update && apk add --no-cache libsodium

USER        container
ENV         USER=container HOME=/home/container
WORKDIR     /home/container

COPY        ./entrypoint.sh /entrypoint.sh
CMD         ["/bin/ash", "/entrypoint.sh"]